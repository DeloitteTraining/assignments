package exam;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Employ {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the number of Employees");
		int n = sc.nextInt();
		
		EmployVO emp[] = new EmployVO[n];
		EmployBO bo =  new EmployBO();
		
		System.out.println("Enter Employee Details: ");
		for(int i=0; i<n; i++) {
			emp[i] = new EmployVO();
			
			System.out.print("ID: ");
			emp[i].setEmpId(sc.nextInt());
			
			System.out.print("Name: ");
			emp[i].setEmpName(sc.next());
			
			System.out.print("Annual Income: ");
			emp[i].setAnnualIncome(sc.nextInt());
			
			bo.callAnnualIncome(emp[i]);
		}
		
		System.out.println("\nEmployees:");
		emp.toString();
		
		System.out.println("\nEmployees after sorting IT in descending order:");
		Collections.sort(Arrays.asList(emp), new EmploySort());
		emp.toString();;
	}
	
	public static void display(EmployVO[] emp) {
		for(EmployVO e: emp) {
			System.out.println(e);
		

	}

}
}
package exam;

public class EmployVO {

	
	int empId;
	String empName;
	double annualIncome;
	double incomeTax;
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public double getAnnualIncome() {
		return annualIncome;
	}
	public void setAnnualIncome(double annualIncome) {
		this.annualIncome = annualIncome;
	}
	public double getIncomeTax() {
		return incomeTax;
	}
	public void setIncomeTax(double incomeTax) {
		this.incomeTax = incomeTax;
	}
	public EmployVO() {
		// TODO Auto-generated constructor stub
	}
	public EmployVO(int empId, String empName, double annualIncome, double incomeTax) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.annualIncome = annualIncome;
		this.incomeTax = incomeTax;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		
		if(this.getEmpId() != )
		
		
		return super.equals(obj);
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		
		return (this.getEmpId() + " " + this.getEmpName() + " " + this.getAnnualIncome() + " " + this.getIncomeTax());
	}
	
	
	
	
}
package exam;

public class EmployBO {
	
	public static void callAnnualIncome(EmployVO v)
	{
		double a = v.getAnnualIncome();
		if(a < 300000)
		{
			v.setIncomeTax(0);
			return;
			}
			if(v.getAnnualIncome() < 60000)
				{
				v.setIncomeTax(v.getAnnualIncome()*0.10);
				return;
				}
				if(v.getAnnualIncome() < 1000000) {
					v.setIncomeTax(v.getAnnualIncome()*0.15 + 30000);
				}
		}
	}

package exam;

import java.util.Comparator;

public class EmploySort implements Comparator<EmployVO>{

		@Override
		public int compare(EmployVO o1, EmployVO o2) {
			return o1.getIncomeTax() < o2.getIncomeTax() ?	1 : -1;
		}

	}

