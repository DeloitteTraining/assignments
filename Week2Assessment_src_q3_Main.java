package q3;

public class Main {
	public static void main(String[] args) {		
		int n = Integer.parseInt(args[0]);
		int[] x = new int[n];
		for (int i=0; i<n; i++) {
			x[i] = Integer.parseInt(args[i+1]);
		}
		System.out.println(UserMainCode.checkTriplets(x) ? "TRUE" : "FALSE" );
	}
}
import java.util.HashMap;
//import java.util.Map;

public class UserMainCode {

	public static boolean checkTriplets(int[] array) {
		
//		NONCONSEQUTIVE triplets
//		
//		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
//		
//		
//		for (int i : array) {
//			if(map.containsKey(i)) {
//				map.put(i, map.get(i)+1);
//			}else {				
//				map.put(i, 1);
//			}
//		}
		
		int num = Integer.MIN_VALUE,count = 0;
		
		for(int i: array) {
			
			if(num==i) 
				count++;
			else {
				num=i;
				count=1;
			}
			
			if(count==3) 
				return true;
		}
		return false;
	}


}
