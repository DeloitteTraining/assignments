package assignment3;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class EmployeeMain {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the number of Employees");
		int n = sc.nextInt();
		
		EmployeeVo emp[] = new EmployeeVo[n];
		EmployeeBO bo =  new EmployeeBO();
		
		System.out.println("Enter Employee Details: ");
		for(int i=0; i<n; i++) {
			emp[i] = new EmployeeVo();
			
			System.out.print("ID: ");
			emp[i].setEmpId(sc.nextInt());
			
			System.out.print("Name: ");
			emp[i].setEmpName(sc.next());
			
			System.out.print("Annual Income: ");
			emp[i].setAnnualIncome(sc.nextInt());
			
			bo.calIncomeTax(emp[i]);
		}
		
		System.out.println("\nEmployees:");
		display(emp);
		
		System.out.println("\nEmployees after sorting IT in descending order:");
		Collections.sort(Arrays.asList(emp), new EmployeeSort());
		display(emp);
	}
	
	public static void display(EmployeeVo[] emp) {
		for(EmployeeVo e: emp) {
			System.out.println(e);
		}
	}

}

