package assignment3;

public class EmployeeVo {
	int empId;
	String empName;
	int annualIncome;
	int incomeTax;
	
	
	public EmployeeVo() {}
	
	public EmployeeVo(int empId, String empName, int annualIncome, int incomeTax) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.annualIncome = annualIncome;
		this.incomeTax = incomeTax;
	}

	public int getEmpId() {
		return empId;
	}
	public String getEmpName() {
		return empName;
	}
	public int getAnnualIncome() {
		return annualIncome;
	}
	public int getIncomeTax() {
		return incomeTax;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public void setAnnualIncome(int annualIncome) {
		this.annualIncome = annualIncome;
	}
	public void setIncomeTax(int incomeTax) {

		this.incomeTax = incomeTax;
	}
	
		
	
	@Override
	public String toString() {
		return "EmployeeVo [empId=" + empId + ", empName=" + empName + ", annualIncome=" + annualIncome + ", incomeTax="
				+ incomeTax + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + annualIncome;
		result = prime * result + empId;
		result = prime * result + ((empName == null) ? 0 : empName.hashCode());
		result = prime * result + incomeTax;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeVo other = (EmployeeVo) obj;
		if (annualIncome != other.annualIncome)
			return false;
		if (empId != other.empId)
			return false;
		if (empName == null) {
			if (other.empName != null)
				return false;
		} else if (!empName.equals(other.empName))
			return false;
		if (incomeTax != other.incomeTax)
			return false;
		return true;
	}
	
}





