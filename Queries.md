## 1. Display id and name in capital letters where the first letter is r.

### select stud_id, upper(f_name) from s_details where f_name like 'r%';
--


## 2.


--


## 3. Display the number of students born in each month.

### select extract(month from DOB) as Month, count(*) as Count from s_details group by extract(month from DOB) order by extract(month from DOB);
--


## 4. Accept 2 numbers and choice 1,2,3,4. Add, Sub, Mul, Div. Not to use any control structures.


--


## 5. Display all subjects along with the number of students enroled in each subject.

### select s.sub_name, count(*) from sub s, conn c where (s.sub_id = c.subid1 or s.sub_id = c.subid2 or s.sub_id = c.subid3 ) group by s.sub_name;
--


## 6. Display the names of the students whose birthdate is in the following month.

### select f_name, DOB from s_details where (extract(month from DOB) = MOD(extract(month from sysdate)+1,12));
--


## 7. Create a user with your own name and see all the data of the system user by creating a role.


--


## 8. Create a query to display test-wise pass and failures.

### select m.test as Test, (select count(*) from marks where test=m.test)-count(*) as Pass, count(*) as Fail from marks m where (m.m1<35 or m.m2<35 or m.m3<35) group by m.test;
--


## 9. Write a query to display Student name and marks who has taken hindi as a subject.

### select f_name as Name from s_details where stud_id in(select c.stud_id from conn c, sub s where (c.subid1=s.sub_id or c.subid2=s.sub_id or c.subid3=s.sub_id) and s.sub_name='hindi');
--


## 10. Give a count for the subjects - Maths and English (Student count).

### select x.sub_name,count(*) from sub x, conn z where (x.sub_id=z.subid1 or x.sub_id=z.subid2 or x.sub_id=z.subid3) and (x.sub_name='english' or x.sub_name='maths') group by x.sub_name;
--